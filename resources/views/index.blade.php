<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="csrf-token" content="{{ csrf_token() }}">

		<link type = "text/css" rel = "stylesheet" href = "{{ asset('packages/packages.css') }}">

		<title>{{ config('app.name', 'Cannot display page name.') }}</title>
	</head>

	<body>
		@include('includes.navbar-plain')

		<div class = "ui container" style = "padding-top: 6rem; max-height: 1px;">
			<div class = "ui stackable equal width grid">
				<div class = "row">
					<div class = "column">
						<div class = "ui small raised clearing text segment">
							<h3 class = "ui header">
								Reservation form
							</h3>

							<table id = "tblServicesToReserve" class = "ui celled striped form table" cellspacing="0" width="100%">
								<thead>
									<tr>
										<th>#</th>
										<th>Service/s</th>
										<th>Quantity</th>
										<th>Action</th>
									</tr>
								</thead>

								<tbody>

								</tbody>

								<tfoot>
									<tr>
										<td></td>
										<td>
											<select id = "cmb-reserve-services" class = "ui small fluid search dropdown" required>
									
											</select>
										</td>
										<td>
											<input type = "number" id = "nud-reserve-qty" min = "0" step = "1" value = "1" required>
										</td>
										<td>
											<button type = "button" id = "btnAddServiceToReserve" class = "ui mini primary button">
												<i class = "plus icon"></i>
												Add
											</button>
										</td>
									</tr>
								</tfoot>
							</table>

							<form id = "frm-reserve" class = "ui small equal width form">
								{{-- {{ csrf_field() }} --}}
								<input type="hidden" id="token" value="{{ csrf_token() }}">

								<div class = "ui horizontal divider header">
									<i class = "money icon"></i>
									METHOD OF PAYMENT
								</div>
								<div class = "inline field">
									<label>How would you like to pay?</label>
									<select id = "cmb-reserve-payment-method" name = "cmb-reserve-payment-method" class = "ui small search dropdown" required>
										<option value = "Cash">Cash</option>
										<option value = "Check">Check</option>
									</select>
								</div>

								<div id = "div-bank-details" class = "fields">
									<div class = "field">
										<label>Bank name:</label>
										<input type = "text" id = "txt-reserve-bank-name" name = "txt-reserve-bank-name">
									</div>

									<div class = "field">
										<label>Check number:</label>
										<input type = "text" id = "txt-reserve-check-number" name = "txt-reserve-check-number">
									</div>

									<div class = "field">
										<label>Date tendered:</label>
										<input type = "datetime-local" id = "txt-reserve-check-date" name = "txt-reserve-check-date" value = "{{ date("Y-m-d") }}">
									</div>
								</div>

								<div class = "ui horizontal divider header">
									<i class = "user icon"></i>
									PERSONAL INFORMATION
								</div>
								<div class = "fields">
									<div class = "field{{ $errors->has('txt-reserve-fname') ? ' has-txt-reserve-fname' : '' }}">
										<label>First name:</label>
										<input type = "text" id = "txt-reserve-fname" name = "txt-reserve-fname" required>
									</div>

									<div class = "field{{ $errors->has('txt-reserve-lname') ? ' has-txt-reserve-lname' : '' }}">
										<label>Last name:</label>
										<input type = "text" id = "txt-reserve-lname" name = "txt-reserve-lname" required>
									</div>

									<div class = "field{{ $errors->has('txt-reserve-contact-number') ? ' has-txt-reserve-contact-number' : '' }}">
										<label>Contact number:</label>
										<div class = "ui labeled input">
											<div class = "ui label">+63</div>
											<input type = "text" id = "txt-reserve-contact-number" name = "txt-reserve-contact-number" required>
										</div>
									</div>
								</div>

								<div class = "field{{ $errors->has('txt-reserve-address') ? ' has-txt-reserve-address' : '' }}">
									<label>Address:</label>
									<textarea id = "txt-reserve-address" name = "txt-reserve-address" rows = "2" required></textarea>
								</div>

								<div class = "fields">
									<div class = "field">
										<label>Additonal notes:</label>
										<textarea id = "txt-reserve-notes" name = "txt-reserve-notes" rows = "2"></textarea>
									</div>

									<div class = "field">
										<label>Reservation date:</label>
										<input type = "datetime-local" id = "txt-reserve-date" name = "txt-reserve-date" required>
									</div>

									<div class = "field">
										<label>Grand Total:</label>
										<input type = "number" id = "nud-grand-total" name = "nud-grand-total" min = "0" required readonly>
									</div>
								</div>

								<button type = "submit" id = "btn-reserve" name = "btn-reserve" class = "ui small green button">
									<i class = "send icon"></i>
									Place reservation
								</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>

		<script src="{{ asset('packages/jquery/jquery-3.2.1.js') }}"></script>
		<script src="{{ asset('packages/semantic-ui/dist/semantic.js') }}"></script>
		<script src="{{ asset('packages/toastr/build/toastr.min.js') }}"></script>
		<script src="{{ asset('packages/jquery-mask/dist/jquery.mask.js') }}"></script>
		<script src="{{ asset('js/core.js') }}"></script>
		@if(session()->has('status'))
			<script type = "text/javascript">
				toastr.success('{{ session('status') }}');
			</script>
		@endif
	</body>
</html>
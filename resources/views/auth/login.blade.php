@section('page-name')
    {{ config('app.name', 'Cannot display page name') }} | Login
@endsection

@extends("layouts.master")

@section("content")
    <div class = "row">
        <div class = "column"></div>
        <div class = "nine wide column">
            <div class = "ui small text segment">
                <h5 class = "ui header">Login</h5>
                
                <form id = "frmLogin" class = "ui small equal width form" method = "POST" action = "{{ route('login') }}">
                    {{ csrf_field() }}

                    <div class = "field{{ $errors->has('email') ? ' has-email' : '' }}">
                        <label>Email:</label>
                        <input id="email" type="email" name="email" value="{{ old('email') }}" required autofocus>
                    </div>

                    <div class = "field{{ $errors->has('password') ? ' has-password' : '' }}">
                        <label>Password:</label>
                        <input id="password" type="password" name="password" required>
                    </div>

                    <div class = "field">
                        <div class = "ui checkbox">
                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                            <label>Keep me logged in</label>
                        </div>
                    </div>

                    <div class = "field">
                        <button type = "submit" class = "ui small positive button">Login</button>
                    </div>

                    <div class = "field">
                        <a href = "{{ route('password.request') }}">Forgot your password?</a>
                    </div>
                </form>
            </div>
        </div>
        <div class = "column"></div>
    </div>
@endsection

@section('scripts')
    @if($errors->has('email'))
        <script type = "text/javascript">
            toastr.error('{{ $errors->first('email') }}');
        </script>
    @endif

    @if($errors->has('password'))
        <script type = "text/javascript">
            toastr.error('{{ $errors->first('password') }}');
        </script>
    @endif

    <script type = "text/javascript">
        $('#pgLogin').toggleClass('active',true);
    </script>
@endsection
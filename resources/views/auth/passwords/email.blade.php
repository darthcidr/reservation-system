@section('page-name')
    {{ config('app.name', 'Laravel') }} | Reset password
@endsection

@extends('layouts.master')

@section('content')
{{-- <div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Reset Password</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Send Password Reset Link
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> --}}
    <div class = "row">
        <div class = "column"></div>
        <div class = "seven wide column">
            <div class = "ui small text segment">
                <h5 class = "ui header">Reset password</h5>

                <form class="ui small equal width form" method="POST" action="{{ route('password.email') }}">
                    {{ csrf_field() }}

                    <div class="field{{ $errors->has('email') ? ' has-error' : '' }}">
                        <div class="field">
                            <label>E-Mail Address</label>
                            <input id="email" type="email" name="email" value="{{ old('email') }}" required>
                        </div>

                        <button type="submit" class="ui small primary button">Send password reset link</button>
                    </div>
                </form>
            </div>
        </div>
        <div class = "column"></div>
    </div>
@endsection

@section('scripts')
    @if (session('status'))
        <script type = "text/javascript">
            toastr.info('{{ session('status') }}');
        </script>
    @endif

    @if ($errors->has('email'))
        <script type = "text/javascript">
            toastr.error('{{ $errors->first('email') }}');
        </script>
    @endif
@endsection

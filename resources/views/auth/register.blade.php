@section('page-name')
    {{ config('app.name', 'Cannot display page name') }} | Register
@endsection

@extends('layouts.master')

@section('content')
    <div class = "row">
        <div class = "column"></div>
        <div class = "ten wide column">
            <div class = "ui small text segment">
                <h5 class = "ui header">Register</h5>

                <form id = "frmRegister" class = "ui small equal width form" method = "POST" action = "{{ route('register') }}">
                    {{ csrf_field() }}

                    <div class = "fields">
                        <div class = "field{{ $errors->has('fname') ? ' has-fname' : '' }}">
                            <label>First name:</label>
                            <input id="fname" type="text" name="fname" value="{{ old('fname') }}" required autofocus>
                        </div>

                        <div class = "field">
                            <label>Middle name:</label>
                            <input id="mname" type="text" name="mname" value="{{ old('mname') }}" autofocus>
                        </div>

                        <div class = "field{{ $errors->has('lname') ? ' has-lname' : '' }}">
                            <label>Last name:</label>
                            <input id="lname" type="text" name="lname" value="{{ old('lname') }}" required autofocus>
                        </div>
                    </div>

                    <div class = "field{{ $errors->has('email') ? ' has-email' : '' }}">
                        <label>E-mail address:</label>
                        <input id="email" type="email" name="email" value="{{ old('email') }}" required>
                    </div>

                    <div class = "fields">
                        <div class="field{{ $errors->has('password') ? ' has-password' : '' }}">
                            <label>Password:</label>
                            <input id="password" type="password" name="password" required>
                        </div>

                        <div class="field">
                            <label>Confirm password:</label>
                            <input id="password-confirm" type="password" name="password_confirmation" required>
                        </div>
                    </div>

                    <button type = "submit" class = "ui small positive button">Register</button>
                    <button type = "reset" class = "ui small negative button">Reset fields</button>
                </form>
            </div>
        </div>
        <div class = "column"></div>
    </div>
@endsection

@section('scripts')
    @if($errors->has('email'))
        <script type = "text/javascript">
            toastr.error('{{ $errors->first('email') }}')
        </script>
    @endif

    @if($errors->has('fname'))
        <script type = "text/javascript">
            toastr.error('{{ $errors->first('fname') }}')
        </script>
    @endif

    @if($errors->has('lname'))
        <script type = "text/javascript">
            toastr.error('{{ $errors->first('lname') }}')
        </script>
    @endif

    @if($errors->has('password'))
        <script type = "text/javascript">
            toastr.error('{{ $errors->first('password') }}')
        </script>
    @endif

    <script type = "text/javascript">
        $('#pgRegister').toggleClass('active',true);

        $('#password, #password-confirm').on('keyup click', function(){
            if ($('#password').val() != $('#password-confirm').val()) {
                $('#password, #password-confirm').css({
                    'border-color':'red'
                });
            }
            else {
                $('#password, #password-confirm').css({
                    'border-color':''
                });
            }
        });
    </script>
@endsection
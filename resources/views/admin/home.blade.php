@section('page-name')
    {{ config('app.name', 'Cannot display page name') }} | Admin Dashboard
@endsection

@extends('layouts.master')

@section('content')
    <div class = "row">
        <div class = "column">
            <div class = "ui small raised text segment">
                <h4 class = "ui header">LOGGED ADMIN</h4>
                <div class = "ui equal width form">
                    <div class = "inline field">
                        <label>Name:</label>
                        <div>{{ Auth::user()->fname }} {{ trim(Auth::user()->mname," ") }} {{ Auth::user()->lname }}</div>
                    </div>

                    <div class = "inline field">
                        <label>Email address:</label>
                        <div>{{ Auth::user()->email }}</div>
                    </div>

                    <div class = "inline field">
                        <label>Admin since:</label>
                        <div>{{ date("F d, Y",strtotime(Auth::user()->created_at)) }}</div>
                    </div>
                </div>
            </div>
        </div>

        <div class = "column">
            <div class = "ui small raised center aligned text segment">
                <div class="ui tiny statistic">
                    <div class="label">
                        Server time
                    </div>
                    <div id = "server-time" class="value"></div>
                </div>
            </div>
        </div>

        <div class = "column">
            <div class = "ui small raised center aligned text segment">
                <div class = "ui tiny statistic">
                    <div class = "label">
                        Total reservations placed
                    </div>
                    <div class = "value">
                        {{ count($reservations) }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @if(session()->has('success'))
        <script type = "text/javascript">
            toastr.success('{{ session('success') }}');
        </script>
    @endif

    <script type = "text/javascript">
        $('#pgHome').toggleClass('active',true);
    </script>
@endsection

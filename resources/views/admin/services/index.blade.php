@section('page-name')
    {{ config('app.name', 'Cannot display page name') }} | Services
@endsection

@extends('layouts.master')

@section('content')
    <div class = "row">
        <div class = "seven wide column">
            <div class = "ui small raised text segment">
                <h5 class = "ui header">Add service</h5>

                <form id = "frm-add-service" class = "ui small equal width form" action = "{{ route('services.store') }}" method = "post">
                    {{ csrf_field() }}

                    <div class = "field{{ $errors->has('txt-service-desc') ? ' has-txt-service-desc' : '' }}">
                        <label>Description:</label>
                        <input type = "text" id = "txt-service-desc" name = "txt-service-desc" required autofocus>
                    </div>

                    <div class = "field{{ $errors->has('txt-service-price') ? ' has-txt-service-price' : '' }}">
                        <label>Price:</label>
                        <input type = "number" id = "nud-service-price" name = "nud-service-price" min = "0.00" step = "0.01" required>
                    </div>

                    <button type = "submit" id = "btn-add-service" name = "btn-add-service" class = "ui small positive button">
                        <i class = "send icon"></i>
                        Submit
                    </button>
                </form>
            </div>
        </div>

        <div class = "column">
            <div class = "ui small raised text segment">
                <h5 class = "ui header">List of services</h5>

                <table id = "tblListOfServices" class = "ui small celled striped table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Description</th>
                            <th>Price</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @if($errors->has('txt-service-desc'))
        <script type = "text/javascript">
            toastr.error('{{ $errors->first('txt-service-desc') }}');
        </script>
    @endif

    @if($errors->has('nud-service-price'))
        <script type = "text/javascript">
            toastr.error('{{ $errors->first('nud-service-price') }}');
        </script>
    @endif

    @if(session()->has('success'))
        <script type = "text/javascript">
            toastr.success('{{ session('success') }}');
        </script>
    @endif

    <script type = "text/javascript">
        $('#pgServices').toggleClass('active',true);
    </script>
@endsection

@section('page-name')
    {{ config('app.name', 'Cannot display page name') }} | Services
@endsection

@extends('layouts.master')

@section('content')
    <div class = "row">
        <div class = "column"></div>

        <div class = "ten wide column">
            <div class = "ui small raised text segment">
                <h5 class = "ui header">Edit service</h5>

                <form id = "frmDeleteService" action = "{{ route('services.destroy', $service->id) }}" method = "post" hidden>
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                </form>

                <form id = "frm-add-service" class = "ui small equal width form" action = "{{ route('services.update', $service->id) }}" method = "post">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                    <div class = "field{{ $errors->has('txt-service-desc') ? ' has-txt-service-desc' : '' }}">
                        <label>Description:</label>
                        <input type = "text" id = "txt-service-desc" name = "txt-service-desc" value = "{{ $service->desc }}" required autofocus>
                    </div>

                    <div class = "field{{ $errors->has('txt-service-price') ? ' has-txt-service-price' : '' }}">
                        <label>Price:</label>
                        <input type = "number" id = "nud-service-price" name = "nud-service-price" value = "{{ $service->price }}" min = "0.00" step = "0.01" required>
                    </div>

                    <div class = "ui three small buttons">
                        <a href = "/services" role = "button" class = "ui button">
                            <i class = "arrow left icon"></i>
                            Back
                        </a>

                        <button type = "submit" class = "ui positive button">
                            <i class = "send icon"></i>
                            Edit
                        </button>

                        <button type = "button" class = "ui negative button" onclick = "$('#frmDeleteService').submit()">
                            <i class = "trash icon"></i>
                            Delete
                        </button>
                    </div>
                </form>
            </div>
        </div>

        <div class = "column"></div>
    </div>
@endsection

@section('scripts')
    @if($errors->has('txt-service-desc'))
        <script type = "text/javascript">
            toastr.error('{{ $errors->first('txt-service-desc') }}');
        </script>
    @endif

    @if($errors->has('nud-service-price'))
        <script type = "text/javascript">
            toastr.error('{{ $errors->first('nud-service-price') }}');
        </script>
    @endif

    @if(session()->has('success'))
        <script type = "text/javascript">
            toastr.success('{{ session('success') }}');
        </script>
    @endif

    <script type = "text/javascript">
        $('#pgServices').toggleClass('active',true);
    </script>
@endsection

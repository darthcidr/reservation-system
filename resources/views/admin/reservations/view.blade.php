@section('page-name')
    {{ config('app.name', 'Cannot display page name') }} | Reservation #{{ $reservation->id }}
@endsection

@extends('layouts.master')

@section('content')
    <div class = "row">
        <div class = "column"></div>

        <div class = "ten wide column">
            <div class = "ui small raised text segment">
                <h5 class = "ui header">View reservation</h5>

                <form id = "frm-delete-reservation" action = "{{ route('reservations.destroy', $reservation->id) }}" method = "post" hidden>
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                </form>

                <form id = "frm-edit-reservation" class = "ui small equal width form" action = "{{ route('reservations.update', $reservation->id) }}" method = "post">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                    <div class = "inline field">
                        <label>Payment method:</label>
                        <select id = "cmb-reserve-payment-method" name = "cmb-reserve-payment-method" class = "ui small search dropdown" required>
                            <option value = "Cash">Cash</option>
                            <option value = "Check">Check</option>
                        </select>
                    </div>

                    <div class = "fields">
                        <div class = "field{{ $errors->has('txt-reserve-fname') ? ' has-txt-reserve-fname' : '' }}">
                            <label>First name:</label>
                            <input type = "text" id = "txt-reserve-fname" name = "txt-reserve-fname" value = "{{ $reservation->fname }}" required autofocus>
                        </div>

                        <div class = "field{{ $errors->has('txt-reserve-lname') ? ' has-txt-reserve-lname' : '' }}">
                            <label>Last name:</label>
                            <input type = "text" id = "txt-reserve-lname" name = "txt-reserve-lname" value = "{{ $reservation->lname }}" required>
                        </div>

                        <div class = "field{{ $errors->has('txt-reserve-contact-number') ? ' has-txt-reserve-contact-number' : '' }}">
                            <label>Contact number:</label>
                            <input type = "text" id = "txt-reserve-contact-number" name = "txt-reserve-contact-number" value = "{{ $reservation->contact_number }}" required>
                        </div>
                    </div>

                    <div class = "field{{ $errors->has('txt-reserve-address') ? ' has-txt-reserve-address' : '' }}">
                        <label>Address:</label>
                        <textarea id = "txt-reserve-address" name = "txt-reserve-address" rows = "2" required>{{ $reservation->address }}</textarea>
                    </div>

                    <div class = "field{{ $errors->has('txt-reserve-notes') ? ' has-txt-reserve-notes' : '' }}">
                        <label>Additional note/s:</label>
                        <textarea id = "txt-reserve-notes" name = "txt-reserve-notes" rows = "2" required>{{ $reservation->notes }}</textarea>
                    </div>

                    <div class = "field">
                        <label>Service/s availed:</label>
                        <div class = "ui relaxed bulleted list">
                            @if(count($services_to_render) > 0)
                                @foreach($services_to_render as $service_to_render)
                                    <div class = "item">{{ $service_to_render->desc }}, {{ $service_to_render->qty }} person/s, {{ ($service_to_render->price*$service_to_render->qty) }} PHP</div>
                                @endforeach
                            @else
                                <div class = "item">No services to render</div>
                            @endif
                        </div>
                    </div>

                    <div class = "field">
                        <label>Grand total:</label>
                        <div>{{ round($reservation->grand_total,2) }} PHP</div>
                    </div>

                    <div class = "ui four small buttons">
                        <a href = "/reservations" role = "button" class = "ui button">
                            <i class = "arrow left icon"></i>
                            Back
                        </a>

                        <button type = "submit" class = "ui positive button">
                            <i class = "send icon"></i>
                            Edit
                        </button>

                        <button type = "button" id = "btn-delete-reservation" class = "ui negative button">
                            <i class = "trash icon"></i>
                            Delete
                        </button>

                        <a href = "/reservations/{{ $reservation->id }}/receipt" class = "ui small button" target = "_new">
                            <i class = "print icon"></i>
                            Print
                        </a>
                    </div>
                </form>
            </div>
        </div>

        <div class = "column"></div>
    </div>
@endsection

@section('scripts')
    @if($errors->has('txt-reserve-fname'))
        <script type = "text/javascript">
            toastr.error('{{ $errors->first('txt-reserve-fname') }}');
        </script>
    @endif

    @if($errors->has('txt-reserve-lname'))
        <script type = "text/javascript">
            toastr.error('{{ $errors->first('txt-reserve-lname') }}');
        </script>
    @endif

    @if($errors->has('txt-reserve-address'))
        <script type = "text/javascript">
            toastr.error('{{ $errors->first('txt-reserve-address') }}');
        </script>
    @endif

    @if($errors->has('txt-reserve-notes'))
        <script type = "text/javascript">
            toastr.error('{{ $errors->first('txt-reserve-notes') }}');
        </script>
    @endif

    @if($errors->has('cmb-reservation-payment-method'))
        <script type = "text/javascript">
            toastr.error('{{ $errors->first('cmb-reservation-payment-method') }}');
        </script>
    @endif

    @if(session()->has('success'))
        <script type = "text/javascript">
            toastr.success('{{ session('success') }}');
        </script>
    @endif

    <script type = "text/javascript">
        $('#pgReservations').toggleClass('active',true);
        $('#cmb-reserve-payment-method').dropdown('set selected','{{ $reservation->payment_method }}');
    </script>
@endsection

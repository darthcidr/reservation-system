{{-- @if(count($orders) > 0)
	@foreach($orders as $order)
		{{ $order->order_id }}
	@endforeach
@else
	No orders
@endif --}}

@section('page-name')
	{{ config('app.name', 'Laravel') }} - Official Receipt #{{ sprintf('%07d', $reservation->id) }}
@endsection

@extends('layouts.receipt')
@section('report-title')
OFFICIAL RECEIPT #{{ sprintf('%07d', $reservation->id) }}
@endsection

@section('content')
	<div class = "row">
		<div class = "column">
			<div class = "ui small basic segment">
				<div class = "ui small equal width form">
					<div class = "fields">
						<div class = "field">
							<label>Customer name:</label>
							<div>{{ $reservation->fname }} {{ $reservation->lname }}</div>
						</div>

						<div class = "field">
							<label>Contact number:</label>
							<div>{{ $reservation->contact_number }}</div>
						</div>

						{{-- <div class = "field">
							<label>Address:</label>
							<div>{{ $reservation->address }}</div>
						</div> --}}
					</div>

					<div class = "fields">
						<div class = "field">
							<label>Payment method:</label>
							<div>{{ $reservation->payment_method }}</div>
						</div>

						<div class = "field">
							<label>Date/time reserved:</label>
							<div>{{ date("F d, Y, h:i A",strtotime($reservation->date)) }}</div>
						</div>
					</div>

					<div class = "field">
						<label>Additional note/s:</label>
						<div>{{ $reservation->notes }}</div>
					</div>
				</div>
			</div>

			<div class = "ui small basic segment">
				<table id = "tbl-list-of-services-to-render" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th align = "left">Particulars</th>
							<th align = "center">Qty.</th>
							<th align = "right">Total</th>
						</tr>
					</thead>

					<tbody>
						@if(count($services_to_render) > 0)
							@foreach($services_to_render as $service_to_render)
								<tr>
									<td align = "left">
										{{ $service_to_render->desc }}
									</td>

									<td align = "center">
										{{ $service_to_render->qty }}
									</td>

									<td align = "right">
										{{ ($service_to_render->price * $service_to_render->qty)  }}
									</td>
								</tr>
							@endforeach
						@else
							<tr>
								<td>No entry</td>
							</tr>
						@endif
						<tr>
							<td></td>
							<td></td>
							<td align = "right" style = "border-top: 1px solid black;"><strong>Grand total: {{ round($reservation->grand_total,2) }} PHP</strong></td>
						</tr>
						<tr>
							<td align = "center" colspan="3"><em>***NOTHING FOLLOWS***</em></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection
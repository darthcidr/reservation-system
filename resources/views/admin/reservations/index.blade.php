@section('page-name')
    {{ config('app.name', 'Cannot display page name') }} | Reservations
@endsection

@extends('layouts.master')

@section('content')
        <div class = "column">
            <div class = "ui small raised text segment">
                <h5 class = "ui header">List of reservations</h5>

                <table id = "tblListOfReservations" class = "ui small celled striped table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Address</th>
                            <th>Payment method</th>
                            <th>Date placed</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>

                <div class = "ui basic center aligned segment">
                    <button type = "button" class = "ui small button" onclick = "fnRefreshReservations()">
                        <i class = "refresh icon"></i>
                        Refresh
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @if(session()->has('success'))
        <script type = "text/javascript">
            toastr.success('{{ session('success') }}');
        </script>
    @endif

    <script type = "text/javascript">
        $('#pgReservations').toggleClass('active',true);
    </script>
@endsection

{{-- @if(count($orders) > 0)
	@foreach($orders as $order)
		{{ $order->order_id }}
	@endforeach
@else
	No orders
@endif --}}

@section('page-name')
	{{ config('app.name', 'Laravel') }} - Reports | Reservations
@endsection

@extends('layouts.report')
@section('report-title')
LIST OF RESERVATIONS
@endsection

@section('content')
	<div class = "ui basic vertical segment">
		<table border = "1" cellspacing="0" cellpadding="5" width="100%">
			<thead>
				<tr>
					<th>ID</th>
					<th>Customer name</th>
					<th>Address</th>
					<th>Date</th>
				</tr>
			</thead>

			<tbody>
				@if(count($reservations) > 0)
					@foreach($reservations as $reservation)
						<tr>
							<td>{{ sprintf('%07d', $reservation->id) }}</td>
							<td>{{ $reservation->fname }} {{ $reservation->lname }}</td>
							<td>{{ $reservation->address }}</td>
							<td>{{ date('F d, Y',strtotime($reservation->created_at)) }}</td>
						</tr>
					@endforeach
				@else
					<tr>
						<td colspan = "6">No reservations placed yet.</td>
					</tr>
				@endif
			</tbody>
		</table>
	</div>
	{{-- <div class = "ui basic right aligned vertical segment">
		<h3 class = "ui header">
			<div class = "content">
				<div class = "sub header">Total Sales:</div>
				PHP {{ $total_sales }} ({{ strtoupper($total_sales_in_words) }} PESOS only)
			</div>
		</h3>
	</div> --}}

@endsection

{{-- @section('signatories')
	<h4 class = "ui header">Records above are hereby certified true and accurate,</h4>
	<div class = "ui basic vertical segment" style = "margin-top: 5rem;">
		<div class = "ui fluid container">
			<div class = "ui stackable equal width center aligned grid">
				<div class = "row">
					<div class = "column">
						<h4 class = "ui header" style = "border-top: 1px solid black;">
							<div class = "content">
								Mau
								<div class = "sub header">Chief Executive Officer</div>
							</div>
						</h4>
					</div>

					<div class = "column">
						<h4 class = "ui header" style = "border-top: 1px solid black;">
							<div class = "content">
								John Doe
								<div class = "sub header">Internal Accountant</div>
							</div>
						</h4>
					</div>

					<div class = "column">
						<h4 class = "ui header" style = "border-top: 1px solid black;">
							<div class = "content">
								Juan Dela Cruz
								<div class = "sub header">External Accountant</div>
							</div>
						</h4>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection --}}
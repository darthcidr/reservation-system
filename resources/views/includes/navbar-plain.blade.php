<div class = "ui large inverted top fixed borderless menu" style = "background-color: #800000; z-index: 1000">
	<a class = "header item" href = "{{ url('/') }}">
		<i class = "large rebel icon"></i>
		{{ config('app.name','Cannot display page name') }}
	</a>

	@if(Auth::check())
		<div class = "right menu">
			<div class = "ui simple dropdown item">
				<div class = "text">
					Welcome, {{ Auth::user()->fname }}
					<i class = "dropdown icon"></i>
				</div>
				<div class = "menu">
					<a href = "{{ route('logout') }}" id = "btnLogout" class = "item">Logout</a>
				</div>
			</div>
		</div>
	@endif
</div>
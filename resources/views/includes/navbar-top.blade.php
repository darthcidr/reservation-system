<div class = "ui large top fixed menu">
	@if(!Auth::check())
		<a class = "header item" href = "{{ url('/') }}">
			<i class = "large rebel icon"></i>
			{{ config('app.name','Cannot display page name') }}
		</a>

		<div class = "right menu">
			<a id = "pgLogin" class = "item" href = "{{ route('login') }}">Login</a>
			<a id = "pgRegister" class = "item" href = "{{ route('register') }}">Register</a>
		</div>
	@else
		<a class = "header item" href = "{{ url('/') }}">
			<i class = "large empire icon"></i>
		</a>
		<a id = "pgHome" class = "item" href = "/home">Home</a>
		<a id = "pgReservations" class = "item" href = "/reservations">Reservations</a>
		<a id = "pgServices" class = "item" href = "/services">Services</a>
		<div class = "ui simple dropdown item">
			<div class = "text">
				Reports
				<i class = "dropdown icon"></i>
			</div>
			<div class = "menu">
				<a class = "item" href = "/reports/reservations" target = "_new">Reservations</a>
			</div>
		</div>

		<div class = "right menu">
			<div class = "ui simple dropdown item">
				<div class = "text">
					Welcome, {{ Auth::user()->fname }}
					<i class = "dropdown icon"></i>
				</div>
				<div class = "menu">
					<a href = "{{ route('logout') }}" id = "btnLogout" class = "item">Logout</a>
				</div>
			</div>
		</div>
	@endif
</div>

@section('scripts')
	
@endsection
<div class = "ui visible inverted vertical borderless sidebar menu" style = "padding-top: 3.5rem;">
	<a id = "pgHome" class = "item" href = "/home">Home</a>
	<a id = "pgReservations" class = "item" href = "/reservations">
		Reservations
		<span id = "reservations-count" class = "ui hidden red label"></span>
	</a>
	<a id = "pgServices" class = "item" href = "/services">Services</a>

	<div class = "item">
		<div class = "ui accordion">
			<a class = "title" style = "color: #fff;">
				Reports
				<i class = "dropdown icon"></i>
			</a>

			<div class = "content">
				<ul style = "list-style: none;">
					<a class = "item" href = "/reports/reservations" target = "_new">Reservations</a>
				</ul>
			</div>
		</div>
	</div>

	{{-- <div class = "item" style = "">
		<div class = "ui mini basic center aligned segment">
			Copyright 2017-{{ date('Y') }} &copy;<br>
			All rights reserved &reg;<br>
			darthjarcas&trade;
		</div>
	</div> --}}
</div>
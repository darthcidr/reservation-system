$('#pgMessages').toggleClass('active');

var tblListOfMessages = $('#tblListOfMessages').DataTable({
	// processing: true,
	ajax:{
		url: '/list/messages',
		dataSrc: '',
		serverside: true
	},
	columns: [
		{'data':'id'},
		{'data':'sender'},
		{'data':'content'},
		{'data':null}
	],
	columnDefs: [
		{
			targets: -1,
			data: null,
			defaultContent: '\
			<button id="btnDeleteMessage" class="ui mini icon button"><i class = "trash icon"></i></button>\
			',
			orderable: false
		},
		{
			searchable: true,
			orderable: true,
			targets: 0
		}
	],
	pageLength: 5,
	deferRender: true
});

setInterval(function(){
	$('#tblListOfMessages').DataTable().ajax.reload(null,false);
},1500);

function fnSendMessage() {
	event.preventDefault();
	$.ajax({
		url: '/message/send',
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		method: 'post',
		data: {
			'token':$('#token-chat').val(),
			'txt-chat-sender':$('#txt-chat-sender').val(),
			'txt-chat-content':$('#txt-chat-content').val()
		},
		dataType: 'json',
		success: function (result) {
			if (result.sent == true)	{
				$('#txt-chat-content').val('');
			}
			else {
				toastr.error('Your message was not sent.');
			}
			// console.log(result.sent);
		}
	});
}
$('#frm-chat').submit(function(event){
	fnSendMessage();
});
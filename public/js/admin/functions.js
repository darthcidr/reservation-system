//get server time
function startTime() {
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
    h = checkTime(h);
    m = checkTime(m);
    s = checkTime(s);
    $('#server-time').html('<i class = "fa fa-clock-o"></i> '+(h%12) + ":" + m + ":" + s + ' ' +ampm(h));
    var t = setTimeout(startTime, 500);
}
function checkTime(i) {
    if (i < 10) {
    	i = "0" + i;
    }  // add zero in front of numbers < 10
    return i;
}
function ampm(h) {
	if (h < 12) {
		return 'AM';
	}
	else {
		return 'PM';
	}
}
startTime();

var tblListOfServices = $('#tblListOfServices').DataTable({
    // processing: true,
    ajax:{
        url: '/tables/services',
        dataSrc: '',
        serverside: true
    },
    columns: [
        {'data':'id'},
        {'data':'desc'},
        {'data':'price'},
        {'data':null}
    ],
    columnDefs: [{
        targets: -1,
        data: null,
        defaultContent: '\
            <button id="btnViewService" class="ui mini icon button"><i class = "eye icon"></i></button>\
        ',
        orderable: false
    },{
        searchable: true,
        orderable: true,
        targets: 0
    }],
    pageLength: 5,
    deferRender: true
});
$('#tblListOfServices tbody').on('click', '#btnViewService', function(event){
    var dataSelect = tblListOfServices.row( $(this).parents('tr') ).data();
    var id = dataSelect['id'];
    self.location = ('/services/'+id);
});

var tblListOfReservations = $('#tblListOfReservations').DataTable({
    // processing: true,
    ajax:{
        url: '/tables/reservations',
        dataSrc: '',
        serverside: true
    },
    columns: [
        {'data':'id'},
        {'data':'fullname',
            render: function(data, type, fullname, meta){
                return fullname.fname + ' ' + fullname.lname;
            }
        },
        {'data':'address'},
        {'data':'payment_method'},
        {'data':'created_at'},
        {'data':null}
    ],
    columnDefs: [{
        targets: -1,
        data: null,
        defaultContent: '\
            <button id="btnViewReservation" class="ui mini icon button"><i class = "eye icon"></i></button>\
        ',
        orderable: false
    },{
        searchable: true,
        orderable: true,
        targets: 0
    }],
    pageLength: 5,
    deferRender: true
});
$('#tblListOfReservations tbody').on('click', '#btnViewReservation', function(event){
    var dataSelect = tblListOfReservations.row( $(this).parents('tr') ).data();
    var id = dataSelect['id'];
    self.location = ('/reservations/view/'+id);
});
$(document).on('click', '#btn-delete-reservation', function(event){
    if (confirm('Confirm delete?') == true) {
        event.preventDefault();
        $('#frm-delete-reservation').submit();
    }
});

function fnGetReservationsCount() {
    $.ajax({
        url: '/reservations/count',
        method: 'get',
        cache: false,
        dataType: 'html',
        success: function(data){
            if (data == 0) {
                $('#reservations-count').toggleClass('hidden',true);
            }
            else if (data != undefined) {
                $('#reservations-count').toggleClass('hidden',false);
                $('#reservations-count').html(data);
            }
            else {
                toastr.error('Cannot retrieve reservations count');
            }
            // console.log(data);
        }
    });
}
fnGetReservationsCount();

function fnRefreshReservations() {
    tblListOfReservations.ajax.reload();
    fnGetReservationsCount();
    console.log('Refreshed');
}
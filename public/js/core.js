//initializes semantic ui dropdown
$('.ui.dropdown').not('.ui.simple.dropdown.item').dropdown();

//initializes semantic ui accordion
$('.ui.accordion').accordion();

//smooth scrolling...is annie okay?
$('a[href*="#"]')
	// Remove links that don't actually link to anything
	.not('[href="#"]')
	.not('[href="#0"]')
	.click(function(event) {
	// On-page links
	if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
		// Figure out element to scroll to
		var target = $(this.hash);
		target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
		// Does a scroll target exist?
		if (target.length) {
			// Only prevent default if animation is actually gonna happen
			event.preventDefault();
			$('html, body').animate({
				scrollTop: target.offset().top
			}, 1000);
		}
	}
});

//toastr option configuration
toastr.options = {
	'closeButton': true,
	'debug': false,
	'newestOnTop': true,
	'progressBar': true,
	'positionClass': 'toast-bottom-right',
	'preventDuplicates': false,
	'onclick': null,
	'showDuration': '300',
	'hideDuration': '1000',
	'timeOut': '3000',
	'extendedTimeOut': '1000',
	'showEasing': 'swing',
	'hideEasing': 'linear',
	'showMethod': 'fadeIn',
	'hideMethod': 'fadeOut'
}

//set number of buttons in data table pagination
// $.fn.DataTable.ext.pager.numbers_length = 5;

function arrSum(array) {
	var sum = 0;
	for (var i = 0; i < array.length; i++) {
		if (typeof array[i]['serviceprice'] == 'object') {
			sum += arrSum((parseFloat(array[i]['serviceprice']) * parseFloat(array[i]['serviceqty'])));
		}
		else {
			sum += (parseFloat(array[i]['serviceprice']) * parseFloat(array[i]['serviceqty']));
		}
	}
	return sum;
}
//get meals for dropdown in order form
$.ajax({
	url: '/lists/services',
	method: 'get',
	cache: false,
	dataType: 'json',
	success: function (data) {
		if (data != undefined) {
			var array = [];
			$.each(data, function(i,val){
				array.push('<option value = "'+val.id+'" data-price = "'+val.price+'">'+val.desc+' (PHP '+val.price+')</option>');
			});
			$('#cmb-reserve-services').html(array);
		}
		else {
			toastr.error('Error loading services.');
		}
	}
});

//reservation placement mechanism
var toreserve = [];
var tempID = 0;
$('#tblServicesToReserve tfoot').on('click', '#btnAddServiceToReserve', function(){
	$('#tblServicesToReserve tbody').empty();
	var serviceid 		= $(this).closest('tr').find('#cmb-reserve-services option:selected').val();
	var servicedesc		= $(this).closest('tr').find('#cmb-reserve-services option:selected').text();
	var serviceprice	= $(this).closest('tr').find('#cmb-reserve-services option:selected').data('price');
	var serviceqty		= $(this).closest('tr').find('#nud-reserve-qty').val();
	
	tempID++;
	selected = {
		'tempID'		: tempID,
		'serviceid'		: serviceid,
		'servicedesc'	: servicedesc,
		'serviceprice'	: serviceprice,
		'serviceqty'	: serviceqty
	};
	toreserve.push(selected);
	$.each(toreserve, function(i,val){
		$('#tblServicesToReserve tbody').append(
			'<tr>'+
				'<td>'+val.tempID+'</td>'+
				'<td>'+val.servicedesc+'</td>'+
				'<td>'+val.serviceqty+'</td>'+
				'<td><button type = "button" id = "btnRemoveServiceToReserve" class = "ui mini button"><i class = "trash icon"></i> Remove</button></td>'+
			'</tr>'
		);
	});
	console.log(toreserve);
	console.log(arrSum(toreserve));
	$('#nud-grand-total').val(arrSum(toreserve));

	//reset everything
	$(this).closest('tr').find('#cmb-reserve-services').dropdown('refresh');
	$(this).closest('tr').find('#nud-reserve-qty').val(1);
});
//delete appended row to tblServicesToReserve
$('#tblServicesToReserve tbody').on('click','#btnRemoveServiceToReserve', function(event){
	var todelete = $(this).closest('tr').remove();
	$.each(toreserve,function(i,val){
		if (toreserve[i].tempID == val.tempID) {
			toreserve.splice(i,1);
			return false;
		}
	});
	$('#nud-grand-total').val(arrSum(toreserve));
	// toassign.splice(id,1);
});
$('#frm-reserve').submit(function(event){
	event.preventDefault();
	$('#txt-reserve-contact-number').unmask();
	$.ajax({
		url: '/reservations/place-reservation',
		method: 'post',
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		data: {
			'token'							: $('#token').val(),
			'txt-reserve-fname'				: $('#txt-reserve-fname').val(),
			'txt-reserve-lname'				: $('#txt-reserve-lname').val(),
			'txt-reserve-address'			: $('#txt-reserve-address').val(),
			'txt-reserve-notes'				: $('#txt-reserve-notes').val(),
			'cmb-reserve-payment-method'	: $('#cmb-reserve-payment-method option:selected').val(),
			'txt-reserve-contact-number'	: $('#txt-reserve-contact-number').val(),
			'txt-reserve-date'				: $('#txt-reserve-date').val(),
			'nud-grand-total'				: $('#nud-grand-total').val(),
			'toreserve'						: toreserve
		},
		dataType: 'json',
		beforeSend: function(xhr) {
			if (toreserve.length == 0) {
				xhr.abort();
				toastr.error('Please order a meal first.');
			}
		},
		success: function (result) {
			if (result["saved"] == true) {
				// toreserve = [];
				// $('#tblServicesToReserve tbody').empty();
				// $('.ui.search.dropdown').dropdown('refresh');
				// $('#frm-reserve')[0].reset();
				// $('#txt-reserve-contact-number').mask('999-999-9999');

				// toastr.success(result["message"]);

				self.location = '/reservations/place-reservation/successful';
			}
			else {
				// toastr.error(result["message"]);
				self.location = '/reservations/place-reservation/failed';
			}
		}
	});
});

//payment method
$('#div-bank-details').hide();
$(document).on('change', '#cmb-reserve-payment-method', function(event){
	event.preventDefault();
	if ($(this).val() == "Cash") {
		$('#div-bank-details').hide();
	}
	else if ($(this).val() == "Check") {
		$('#div-bank-details').show();
	}
});
//set username
$(document).on('click', '#btn-set-username', function(event){
	event.preventDefault();
	var username = prompt('Please type your username:');
	if (username != '') {
		$('#txt-chat-sender').val(username);
	}
	else {
		toastr.error('Please set your username to enable chat.');
	}
});

function fnGetMessages() {
	$.ajax({
		url: '/list/messages',
		method: 'get',
		cache: false,
		dataType: 'json',
		success: function (data) {
			if (data != undefined) {
				$('#div-chat-messages').empty();
				var array = [];
				$.each(data, function(i,val){
					array.push(
						'<h5 class = "ui header" style = "font-weight: normal;">'+
							'<div class = "content">'+
								val.content+
								'<div class = "sub header">by <em>'+val.sender+'</em>, '+val.created_at+'</div>'+
							'</div>'+
						'</h5>'+
						'<div class = "ui divider"></div>'
					);
					// array.push(
					// 	'<div class="event">'+
					// 		'<div class="content">'+
					// 			'<div class="summary">'+
					// 				'<strong>'+val.sender+'</strong> said'+
					// 			'</div>'+
					// 			'<div class="extra text">'+
					// 				val.content+
					// 			'</div>'+
					// 			'<div class="date">Sent at '+
					// 				val.created_at+
					// 			'</div>'+
					// 		'</div>'+
					// 	'</div>'+
					// 	'<div class = "ui divider"></div>'
					// );
				});
				$('#div-chat-messages').html(array);
			}
			else {
				toastr.error('Unable to retrieve messages.');
			}
			// console.log(data);
		}
	});
}
fnGetMessages();
setInterval(function(){
	fnGetMessages();
},1000);

function fnSendMessage() {
	event.preventDefault();
	$.ajax({
		url: '/message/send',
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		method: 'post',
		data: {
			'token':$('#token-chat').val(),
			'txt-chat-sender':$('#txt-chat-sender').val(),
			'txt-chat-content':$('#txt-chat-content').val()
		},
		dataType: 'json',
		success: function (result) {
			if (result.sent == true)	{
				$('#txt-chat-content').val('');
				fnGetMessages();
			}
			else {
				toastr.error('Your message was not sent.');
			}
			// console.log(result.sent);
		}
	});
}
$('#frm-chat').submit(function(event){
	fnSendMessage();
});
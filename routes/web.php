<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

// Auth::routes();
Route::get('/admin/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/admin/login', 'Auth\LoginController@login');
Route::post('/admin/logout', 'Auth\LoginController@logout')->name('logout');
// Registration Routes
Route::get('/admin/register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('/admin/register', 'Auth\RegisterController@register');
// Password Reset Routes
Route::get('/admin/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('/admin/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('/admin/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('/admin/password/reset', 'Auth\ResetPasswordController@reset');
// Admin Dashboard Route
Route::get('/admin/home', 'HomeController@index')->middleware('auth')->name('home');
// Logout Route
Route::get('/admin/logout', 'Auth\LoginController@logout', function(){
	return view('index')->with('success','You have been logged out of your account.');
});
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::get('/home', 'HomeController@index')->name('home');

//tables
Route::get('/tables/services', 'TablesController@services')->middleware('auth');
Route::get('/tables/reservations', 'TablesController@reservations')->middleware('auth');

//dropdowns
Route::get('/lists/services', 'DropdownsController@services');

//services
Route::resource('/services', 'ServicesController');

//reservations
Route::get('/reservations', 'ReservationsController@index')->middleware('auth');
Route::get('/reservations/view/{reservation}', 'ReservationsController@show');
Route::post('/reservations/place-reservation', 'ReservationsController@store');
Route::delete('/reservations/{reservation}', 'ReservationsController@destroy')->name('reservations.destroy')->middleware('auth');
Route::put('/reservations/{reservation}', 'ReservationsController@update')->name('reservations.update')->middleware('auth');
Route::get('/reservations/{reservation}/receipt', 'ReportsController@reservation_receipt');
Route::get('/reservations/place-reservation/successful', function(){
	return redirect('/')->with('status','Reservation placement successful.');
});
Route::get('/reservations/place-reservation/failed', function(){
	return redirect('/')->with('status','Reservation placement failed.');
});
Route::get('/reservations/count', 'ReservationsController@count')->middleware('auth');

//SMS Notifications using Nexmo SMS API
// Route::get('/sms/send/{number}/{name}/{date}', function(\Nexmo\Client $nexmo, $number, $name, $date) {
// 	$message = $nexmo->message()->send([
// 		'to' => $number,
// 		'from' => '@leggetter',
// 		'text' => 'Hello, '.$name.' Sent at '.$date
// 	]);
// 	Log::info('sent message: ' . $message['message-id']);

// 	return 'Message '.$message['message-id'].' successfully sent.';

// });

//reports
Route::get('/reports/reservations', 'ReportsController@reservations')->middleware('auth');
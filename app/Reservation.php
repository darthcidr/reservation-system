<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $fillable = [
    	"txt-reserve-fname",
    	"txt-reserve-lname",
    	"txt-reserve-address",
    	"txt-reserve-notes",
    	"cmb-reserve-payment-method",
    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceToRender extends Model
{
	public $table = "services_to_render";

    protected $fillable = [
    	"toreserve"
    ];
}

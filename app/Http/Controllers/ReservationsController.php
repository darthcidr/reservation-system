<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Nexmo;

use DB;

use App\Reservation;
use App\ServiceToRender;

class ReservationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.reservations.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $saved = false;
        $reservation = new Reservation;
        $reservation->fname             = $request["txt-reserve-fname"];
        $reservation->lname             = $request["txt-reserve-lname"];
        $reservation->contact_number    = "+63".$request["txt-reserve-contact-number"];
        $reservation->address           = $request["txt-reserve-address"];
        $reservation->notes             = $request["txt-reserve-notes"];
        $reservation->payment_method    = $request["cmb-reserve-payment-method"];
        $reservation->date              = $request["txt-reserve-date"];
        $reservation->grand_total       = $request["nud-grand-total"];
        $reservation->save();

        $saved = $reservation->save();
        $last_inserted = $reservation->id;

        // for($i = 0 ; $i < count($request["toreserve"]) ; $i++) {
        //     $service_to_render = new ServiceToRender;
        //     $service_to_render->reservation_id = $last_inserted;
        //     $service_to_render->service_id = $request["toreserve"][$i]["serviceid"];
        //     $service_to_render->qty = $request["toreserve"][$i]["serviceqty"];
        //     $saved = $service_to_render->save();
        // }

        // return response()->json([
        //     "saved"     =>  $saved,
        //     "message"   =>  "Reservation placement successful."
        // ]);

        if ($saved == true) {
            for($i = 0 ; $i < count($request["toreserve"]) ; $i++) {
                $service_to_render = new ServiceToRender;
                $service_to_render->reservation_id = $last_inserted;
                $service_to_render->service_id = $request["toreserve"][$i]["serviceid"];
                $service_to_render->qty = $request["toreserve"][$i]["serviceqty"];
                $saved = $service_to_render->save();
            }

            if ($saved == true) {
                $notification = Nexmo::message()->send([
                    'to' => '+63'.$request["txt-reserve-contact-number"],
                    'from' => '@leggetter',
                    'text' => 'Good day, Mr/Ms '.strtoupper($request["txt-reserve-fname"]).' '.strtoupper($request["txt-reserve-lname"]).'. This is to notify you about your reservation under Reservation #'.sprintf('%07d', $last_inserted).' scheduled on '.date('F d, Y h:i A',strtotime($request["txt-reserve-date"])).'. Thank you for your patronage!'
                ]);
                \Log::info('sent message: ' . $notification['message-id']);
            }
            else {
                return response()->json([
                    "saved"     =>  $saved,
                    "message"   =>  "Reservation placement failed."
                ]);
            }
        }
        else {
            return response()->json([
                "saved"     =>  $saved,
                "message"   =>  "Reservation placement failed."
            ]);
        }

        return response()->json([
            "saved"     =>  $saved,
            "message"   =>  "Reservation placement successful."
        ]);
}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $reservation = Reservation::find($id);
        // $services_to_render = ServiceToRender::where('reservation_id',$id)->get();
        $services_to_render = DB::SELECT("
            SELECT
                services_to_render.id,
                services_to_render.reservation_id,

                services_to_render.service_id,
                services.desc,
                services.price,

                services_to_render.qty,
                services_to_render.created_at
            FROM
                services_to_render
                    INNER JOIN services ON services_to_render.service_id = services.id
            WHERE
                services_to_render.reservation_id = '".$id."';
        ");

        $reservation_total = DB::SELECT("
            SELECT
                SUM(services.price*services_to_render.qty) AS 'grand_total'
            FROM
                services_to_render
                    INNER JOIN services ON services_to_render.service_id = services.id
            WHERE
                services_to_render.reservation_id = '".$id."'
            GROUP BY
                services_to_render.reservation_id;
        ");

        return view('admin.reservations.view')->with([
            'reservation'           =>  $reservation,
            'services_to_render'    =>  $services_to_render,
            'reservation_total'     =>  $reservation_total
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $reservation = Reservation::find($id);
        $reservation->fname = $request["txt-reserve-fname"];
        $reservation->lname = $request["txt-reserve-lname"];
        $reservation->address = $request["txt-reserve-address"];
        $reservation->notes = $request["txt-reserve-notes"];
        $reservation->payment_method = $request["cmb-reserve-payment-method"];
        $reservation->update();

        return redirect('/reservations')->with('success','Reservation updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = false;
        $reservation = Reservation::find($id);
        $deleted = $reservation->delete();
        $services_to_render = ServiceToRender::where('reservation_id',$id);
        $deleted = $services_to_render->delete();

        return redirect('/reservations')->with('success','Reservation has been deleted.');
    }

    public function count()
    {
        $reservations = Reservation::count();

        return $reservations;
    }
}

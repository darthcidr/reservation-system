<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

use App\Reservation;
use App\ServiceToRender;

class ReportsController extends Controller
{
    public function reservations()
    {
    	$reservations = Reservation::all();

    	return view('admin.reports.reservations')->with('reservations',$reservations);
    }

    public function reservation_receipt($id)
    {
    	$reservation = Reservation::find($id);

    	$services_to_render = DB::SELECT("
            SELECT
                services_to_render.id,
                services_to_render.reservation_id,

                services_to_render.service_id,
                services.desc,
                services.price,

                services_to_render.qty,
                services_to_render.created_at
            FROM
                services_to_render
                    INNER JOIN services ON services_to_render.service_id = services.id
            WHERE
                services_to_render.reservation_id = '".$id."';
        ");

    	return view('admin.reservations.receipt')->with([
    		'reservation'			=>	$reservation,
    		'services_to_render'	=>	$services_to_render
    	]);
    }
}

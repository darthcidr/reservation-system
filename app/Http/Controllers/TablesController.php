<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Service;
use App\Reservation;

class TablesController extends Controller
{
    public function services()
    {
    	$services = Service::all();

    	return $services;
    }

    public function reservations()
    {
    	$reservations = Reservation::all();

    	return $reservations;
    }
}
